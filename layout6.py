from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication,QWidget, QVBoxLayout, QPushButton, QFileDialog , QLabel, QTextEdit, QMessageBox
from PyQt5.QtGui import QPixmap
import sys

# remove warning message
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# required library
import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pytesseract
import imutils
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
from local_utils import detect_lp
from os.path import splitext,basename
from keras.models import model_from_json
from keras.preprocessing.image import load_img, img_to_array
from keras.applications.mobilenet_v2 import preprocess_input
from sklearn.preprocessing import LabelEncoder
import glob

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(2300, 900)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.Imagelabel = QtWidgets.QLabel(self.centralwidget)
        self.Imagelabel.setEnabled(True)
        self.Imagelabel.setGeometry(QtCore.QRect(40, 0, 341, 271))
        self.Imagelabel.setFrameShape(QtWidgets.QFrame.Box)
        self.Imagelabel.setFrameShadow(QtWidgets.QFrame.Plain)
        self.Imagelabel.setLineWidth(4)
        self.Imagelabel.setMidLineWidth(0)
        self.Imagelabel.setText("")
        self.Imagelabel.setPixmap(QtGui.QPixmap("D:\logo.jpg"))
        self.Imagelabel.setScaledContents(True)
        self.Imagelabel.setObjectName("Imagelabel")
        self.Platelabel = QtWidgets.QLabel(self.centralwidget)
        self.Platelabel.setEnabled(True)
        self.Platelabel.setGeometry(QtCore.QRect(490, 30, 361, 211))
        self.Platelabel.setFrameShape(QtWidgets.QFrame.Box)
        self.Platelabel.setLineWidth(3)
        self.Platelabel.setText("")
        self.Platelabel.setScaledContents(True)
        self.Platelabel.setObjectName("Platelabel")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setEnabled(True)
        self.label_3.setGeometry(QtCore.QRect(460, 240, 141, 41))
        self.label_3.setObjectName("label_3")
        self.GetPlateLabel = QtWidgets.QLabel(self.centralwidget)
        self.GetPlateLabel.setEnabled(True)
        self.GetPlateLabel.setGeometry(QtCore.QRect(490, 0, 141, 41))
        self.GetPlateLabel.setObjectName("GetPlateLabel")
        self.BrowseImage = QtWidgets.QPushButton(self.centralwidget)
        self.BrowseImage.setGeometry(QtCore.QRect(110, 280, 171, 41))
        self.BrowseImage.setObjectName("BrowseImage")
        self.GetPlate = QtWidgets.QPushButton(self.centralwidget)
        self.GetPlate.setGeometry(QtCore.QRect(610, 250, 141, 41))
        self.GetPlate.setObjectName("GetPlate")
        self.Dminlabel = QtWidgets.QLabel(self.centralwidget)
        self.Dminlabel.setGeometry(QtCore.QRect(970, 90, 61, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.Dminlabel.setFont(font)
        self.Dminlabel.setObjectName("Dminlabel")
        self.Dmaxlabel = QtWidgets.QLabel(self.centralwidget)
        self.Dmaxlabel.setGeometry(QtCore.QRect(970, 170, 61, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.Dmaxlabel.setFont(font)
        self.Dmaxlabel.setObjectName("Dmaxlabel")
        self.Dmin = QtWidgets.QLineEdit(self.centralwidget)
        self.Dmin.setGeometry(QtCore.QRect(890, 90, 71, 41))
        self.Dmin.setObjectName("Dmin")
        self.Dmax = QtWidgets.QLineEdit(self.centralwidget)
        self.Dmax.setGeometry(QtCore.QRect(890, 170, 71, 41))
        self.Dmax.setObjectName("Dmax")
        self.OriginalPlate = QtWidgets.QLabel(self.centralwidget)
        self.OriginalPlate.setGeometry(QtCore.QRect(50, 320, 261, 211))
        self.OriginalPlate.setFrameShape(QtWidgets.QFrame.Box)
        self.OriginalPlate.setLineWidth(3)
        self.OriginalPlate.setText("")
        self.OriginalPlate.setScaledContents(True)
        self.OriginalPlate.setObjectName("OriginalPlate")
        self.Blur = QtWidgets.QLabel(self.centralwidget)
        self.Blur.setGeometry(QtCore.QRect(180, 580, 261, 211))
        self.Blur.setFrameShape(QtWidgets.QFrame.Box)
        self.Blur.setLineWidth(3)
        self.Blur.setText("")
        self.Blur.setScaledContents(True)
        self.Blur.setObjectName("Blur")
        self.Gray = QtWidgets.QLabel(self.centralwidget)
        self.Gray.setGeometry(QtCore.QRect(330, 320, 261, 211))
        self.Gray.setFrameShape(QtWidgets.QFrame.Box)
        self.Gray.setLineWidth(3)
        self.Gray.setText("")
        self.Gray.setScaledContents(True)
        self.Gray.setObjectName("Gray")
        self.Binary = QtWidgets.QLabel(self.centralwidget)
        self.Binary.setGeometry(QtCore.QRect(460, 580, 261, 211))
        self.Binary.setFrameShape(QtWidgets.QFrame.Box)
        self.Binary.setLineWidth(3)
        self.Binary.setText("")
        self.Binary.setScaledContents(True)
        self.Binary.setObjectName("Binary")
        self.Dilation = QtWidgets.QLabel(self.centralwidget)
        self.Dilation.setGeometry(QtCore.QRect(610, 320, 261, 211))
        self.Dilation.setFrameShape(QtWidgets.QFrame.Box)
        self.Dilation.setLineWidth(3)
        self.Dilation.setText("")
        self.Dilation.setScaledContents(True)
        self.Dilation.setObjectName("Dilation")
        self.BlurLabel = QtWidgets.QLabel(self.centralwidget)
        self.BlurLabel.setGeometry(QtCore.QRect(300, 790, 21, 16))
        self.BlurLabel.setObjectName("BlurLabel")
        self.BinaryLabel = QtWidgets.QLabel(self.centralwidget)
        self.BinaryLabel.setGeometry(QtCore.QRect(570, 790, 41, 16))
        self.BinaryLabel.setObjectName("BinaryLabel")
        self.DilationLabel = QtWidgets.QLabel(self.centralwidget)
        self.DilationLabel.setGeometry(QtCore.QRect(740, 530, 41, 16))
        self.DilationLabel.setObjectName("DilationLabel")
        self.OriginalPlateLabel = QtWidgets.QLabel(self.centralwidget)
        self.OriginalPlateLabel.setGeometry(QtCore.QRect(110, 530, 121, 16))
        self.OriginalPlateLabel.setObjectName("OriginalPlateLabel")
        self.GrayLabel = QtWidgets.QLabel(self.centralwidget)
        self.GrayLabel.setGeometry(QtCore.QRect(460, 530, 31, 16))
        self.GrayLabel.setObjectName("GrayLabel")
        self.PSM = QtWidgets.QLineEdit(self.centralwidget)
        self.PSM.setGeometry(QtCore.QRect(920, 370, 81, 51))
        self.PSM.setObjectName("PSM")
        self.PSMLabel = QtWidgets.QLabel(self.centralwidget)
        self.PSMLabel.setGeometry(QtCore.QRect(1020, 390, 61, 16))
        self.PSMLabel.setObjectName("PSMLabel")
        self.GetResult = QtWidgets.QPushButton(self.centralwidget)
        self.GetResult.setGeometry(QtCore.QRect(960, 440, 101, 41))
        self.GetResult.setObjectName("GetResult")
        self.Final = QtWidgets.QLabel(self.centralwidget)
        self.Final.setGeometry(QtCore.QRect(910, 570, 91, 16))
        self.Final.setObjectName("Final")
        self.OriginalPlateLabel_2 = QtWidgets.QLabel(self.centralwidget)
        self.OriginalPlateLabel_2.setGeometry(QtCore.QRect(770, 600, 121, 16))
        self.OriginalPlateLabel_2.setObjectName("OriginalPlateLabel_2")
        self.OriginalPlateResult = QtWidgets.QLineEdit(self.centralwidget)
        self.OriginalPlateResult.setGeometry(QtCore.QRect(900, 600, 201, 22))
        self.OriginalPlateResult.setObjectName("OriginalPlateResult")
        self.GrayLabel_2 = QtWidgets.QLabel(self.centralwidget)
        self.GrayLabel_2.setGeometry(QtCore.QRect(810, 630, 31, 16))
        self.GrayLabel_2.setObjectName("GrayLabel_2")
        self.GrayResult = QtWidgets.QLineEdit(self.centralwidget)
        self.GrayResult.setGeometry(QtCore.QRect(900, 630, 201, 22))
        self.GrayResult.setObjectName("GrayResult")
        self.BlurLabel_2 = QtWidgets.QLabel(self.centralwidget)
        self.BlurLabel_2.setGeometry(QtCore.QRect(810, 660, 21, 16))
        self.BlurLabel_2.setObjectName("BlurLabel_2")
        self.BlurResult = QtWidgets.QLineEdit(self.centralwidget)
        self.BlurResult.setGeometry(QtCore.QRect(900, 660, 201, 22))
        self.BlurResult.setObjectName("BlurResult")
        self.BinaryLabel_2 = QtWidgets.QLabel(self.centralwidget)
        self.BinaryLabel_2.setGeometry(QtCore.QRect(810, 690, 41, 16))
        self.BinaryLabel_2.setObjectName("BinaryLabel_2")
        self.BinaryResult = QtWidgets.QLineEdit(self.centralwidget)
        self.BinaryResult.setGeometry(QtCore.QRect(900, 690, 201, 22))
        self.BinaryResult.setObjectName("BinaryResult")
        self.DilationLabel_2 = QtWidgets.QLabel(self.centralwidget)
        self.DilationLabel_2.setGeometry(QtCore.QRect(810, 720, 41, 16))
        self.DilationLabel_2.setObjectName("DilationLabel_2")
        self.DilationResult = QtWidgets.QLineEdit(self.centralwidget)
        self.DilationResult.setGeometry(QtCore.QRect(900, 720, 201, 22))
        self.DilationResult.setObjectName("DilationResult")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1124, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
#==========================================================================
        self.ImageLogo = QtWidgets.QLabel(self.centralwidget)
        self.ImageLogo.setGeometry(QtCore.QRect(1400, 0, 350, 250))
        self.ImageLogo.setFrameShape(QtWidgets.QFrame.Box)
        self.ImageLogo.setFrameShadow(QtWidgets.QFrame.Plain)
        self.ImageLogo.setLineWidth(0)
        self.ImageLogo.setMidLineWidth(0)
        self.ImageLogo.setText("")
        self.ImageLogo.setPixmap(QtGui.QPixmap("D:\logo.jpg"))
        self.ImageLogo.setScaledContents(True)
        self.ImageLogo.setObjectName("ImageLogo")

        self.GroupTopic = QtWidgets.QLabel(self.centralwidget)
        self.GroupTopic.setGeometry(QtCore.QRect(1300, 200, 1000, 50))
        self.GroupTopic.setObjectName("GroupTopic")
        MainWindow.setStatusBar(self.statusbar)
        fontTopic = QtGui.QFont()
        fontTopic.setPointSize(20)
        self.GroupTopic.setFont(fontTopic)
        self.GroupTopic.setObjectName("GroupTopic")

        self.GroupMembers = QtWidgets.QLabel(self.centralwidget)
        self.GroupMembers.setGeometry(QtCore.QRect(1200, 300, 1000, 30))
        self.GroupMembers.setObjectName("GroupMembers")
        MainWindow.setStatusBar(self.statusbar)
        fontMember = QtGui.QFont()
        fontMember.setPointSize(20)
        self.GroupMembers.setFont(fontMember)
        self.GroupMembers.setObjectName("GroupMembers")

        self.MemberName1 = QtWidgets.QLabel(self.centralwidget)
        self.MemberName1.setGeometry(QtCore.QRect(1440, 350, 1000, 50))
        self.MemberName1.setObjectName("Member1")
        MainWindow.setStatusBar(self.statusbar)
        fontMember1 = QtGui.QFont()
        fontMember1.setPointSize(13)
        self.MemberName1.setFont(fontMember1)
        self.MemberName1.setObjectName("MemberName1")

        self.MemberName2 = QtWidgets.QLabel(self.centralwidget)
        self.MemberName2.setGeometry(QtCore.QRect(1200, 350, 1000, 50))
        self.MemberName2.setObjectName("Member2")
        MainWindow.setStatusBar(self.statusbar)
        fontMember2 = QtGui.QFont()
        fontMember2.setPointSize(13)
        self.MemberName2.setFont(fontMember2)
        self.MemberName2.setObjectName("MemberName2")

        self.MemberName3 = QtWidgets.QLabel(self.centralwidget)
        self.MemberName3.setGeometry(QtCore.QRect(1680, 350, 1000, 50))
        self.MemberName3.setObjectName("Member3")
        MainWindow.setStatusBar(self.statusbar)
        fontMember3 = QtGui.QFont()
        fontMember3.setPointSize(13)
        self.MemberName3.setFont(fontMember3)
        self.MemberName3.setObjectName("MemberName3")

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "License Plate Recognition"))
        self.label_3.setText(_translate("MainWindow", "Character segmentation"))
        self.GetPlateLabel.setText(_translate("MainWindow", "Get plate here!!!"))
        self.BrowseImage.setText(_translate("MainWindow", "Browse Image"))
        self.BrowseImage.clicked.connect(self.BrowseandShowImage)
        self.GetPlate.setText(_translate("MainWindow", "Get plate"))
        self.GetPlate.clicked.connect(self.ShowLicensePlate)
        self.Dminlabel.setText(_translate("MainWindow", "Dmin"))
        self.Dmaxlabel.setText(_translate("MainWindow", "Dmax"))
        self.BlurLabel.setText(_translate("MainWindow", "Blur"))
        self.BinaryLabel.setText(_translate("MainWindow", "Binary"))
        self.DilationLabel.setText(_translate("MainWindow", "Dilation"))
        self.OriginalPlateLabel.setText(_translate("MainWindow", "Original plate image"))
        self.GrayLabel.setText(_translate("MainWindow", "Gray"))
        self.PSMLabel.setText(_translate("MainWindow", "PSM value"))
        self.GetResult.setText(_translate("MainWindow", "Get Result"))
        self.GetResult.clicked.connect(self.GetPredictionResult)
        self.Final.setText(_translate("MainWindow", "Final Prediction"))
        self.OriginalPlateLabel_2.setText(_translate("MainWindow", "Original plate image"))
        self.GrayLabel_2.setText(_translate("MainWindow", "Gray"))
        self.BlurLabel_2.setText(_translate("MainWindow", "Blur"))
        self.BinaryLabel_2.setText(_translate("MainWindow", "Binary"))
        self.DilationLabel_2.setText(_translate("MainWindow", "Dilation"))
        self.GroupTopic.setText(_translate("MainWindow", "Detect & Recognize License Plate"))
        self.GroupMembers.setText(_translate("MainWindow", "Our Team Members: "))
        self.MemberName1.setText(_translate("MainWindow", "Le Trinh Hoang Phu\n      18110037 "))
        self.MemberName2.setText(_translate("MainWindow", "Huynh Quang Duy\n     18110007 "))
        self.MemberName3.setText(_translate("MainWindow", "Nguyen Dan Truong\n      19110064 "))
    
        #Load wpod model
    def load_model(self,path):
        try:
            path = splitext(path)[0]
            with open('%s.json' % path, 'r') as json_file:
                model_json = json_file.read()
            model = model_from_json(model_json, custom_objects={})
            model.load_weights('%s.h5' % path)
            print("Loading model successfully...")
            return model
        except Exception as e:
            print(e)
    
    def preprocess_image(self, image_path, resize=False):
        img = cv2.imread(image_path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = img / 255
        if resize:
            img = cv2.resize(img, (224,224))
        return img

    def get_plate(self,image_path, Dmax, Dmin): #dmin default 200 dmax default 600
        wpod_net_path = "wpod-net.json" #load wpod model to run the code
        wpod_net = self.load_model(wpod_net_path)
        vehicle = self.preprocess_image(image_path)
        ratio = float(max(vehicle.shape[:2])) / min(vehicle.shape[:2])
        side = int(ratio * Dmin)
        bound_dim = min(side, Dmax)
        _ , LpImg, _, cor = detect_lp(wpod_net, vehicle, bound_dim, lp_threshold=0.5)
        return LpImg
    
    def BrowseandShowImage(self):
        try:
            fname, _= QFileDialog.getOpenFileNames()
            imagePath = fname[0]
            print(imagePath)
            self.label_3.setText(imagePath)
            self.label_3.adjustSize()
            self.label_3.setHidden(True)
            pixmap = QPixmap(imagePath)
            self.Imagelabel.setPixmap(QPixmap(pixmap))   
            self.ImageLogo.setPixmap(QPixmap(pixmap))    
        except Exception as e:
            msg = QMessageBox()
            msg.setWindowTitle("Error!")
            msg.setText("Please select a suitable image!!!")
            msg.setIcon(QMessageBox.Critical)
            e = msg.exec_()
            return

    def ShowLicensePlate(self):
        test_image_path = self.label_3.text()
        print(test_image_path)
        if self.Dmin.text() == "" or self.Dmax.text() == "":
            self.Dmin.insert("200")
            self.Dmax.insert("600")
        try:
            LpImg = self.get_plate(test_image_path, int(self.Dmax.text()), int(self.Dmin.text()))
        except Exception as e:
            self.Platelabel.clear()
            msg = QMessageBox()
            msg.setWindowTitle("Error!")
            msg.setText("Could not find the license plate try to adjust Dmin and Dmax value")
            msg.setIcon(QMessageBox.Critical)
            e = msg.exec_()
            return
        fig = plt.figure(figsize=(12,6))
        grid = gridspec.GridSpec(ncols=1,nrows=1,figure=fig)
        fig.add_subplot(grid[0])
        plt.axis(False)
        plt.imshow(LpImg[0])
        fig.savefig('G:\HK2_Nam 4\Digital\Project\Digital Image Processing Project\18110037_Le Trinh Hoang Phu_18110007_Huynh Quang Duy_19110064_Nguyen Dan Truong\Plate_detect_and_recognize\Fig plot\plate_image.jpg')
        self.GetPlateLabel.setText('Getting image for you...')
        self.GetPlateLabel.adjustSize()
        self.Platelabel.clear()
        self.Platelabel.setPixmap(QPixmap("G:\HK2_Nam 4\Digital\Project\Digital Image Processing Project\18110037_Le Trinh Hoang Phu_18110007_Huynh Quang Duy_19110064_Nguyen Dan Truong\Plate_detect_and_recognize\Fig plot\plate_image.jpg"))
        self.GetPlateLabel.setText('Done!!!')
        self.GetPlateLabel.adjustSize()   

    
    def GetPredictionResult(self):
        test_image_path = self.label_3.text()
        try:
            LpImg = self.get_plate(test_image_path, int(self.Dmax.text()), int(self.Dmin.text()))
        except Exception as e:
            self.OriginalPlate.clear()
            self.Gray.clear()
            self.Blur.clear()
            self.Binary.clear()
            self.Dilation.clear()
            msg = QMessageBox()
            msg.setWindowTitle("Error!")
            msg.setText("Could not pre process the image ! Please add the Dmin and Dmax value")
            msg.setIcon(QMessageBox.Critical)
            e = msg.exec_()
            return
  
        if (len(LpImg)): 
            
            plate_image = cv2.convertScaleAbs(LpImg[0], alpha=(255.0))
            
            
            gray = cv2.cvtColor(plate_image, cv2.COLOR_BGR2GRAY)
            blur = cv2.GaussianBlur(gray,(7,7),0)
            
            
            binary = cv2.threshold(blur, 180, 255,
                                cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
            
            kernel3 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
            dilation = cv2.morphologyEx(binary, cv2.MORPH_DILATE, kernel3)

        # if psm value hasn't been initialized yet
        if(self.PSM.text() == ""):
            self.PSM.insert("10")

        #visualize results in many different filters    
        fig = plt.figure(figsize=(20,10))
        plt.rcParams.update({"font.size":10})
        grid = gridspec.GridSpec(ncols=1,nrows=1,figure = fig)
        fig.add_subplot(grid[0])
        plt.axis(False)
        text = pytesseract.image_to_string(plate_image, config='--psm {0}'.format(int(self.PSM.text())))
        self.OriginalPlateResult.clear()
        self.OriginalPlateResult.insert(text)
        plt.title(text, fontdict={'fontsize' : 80})
        plt.imshow(plate_image)
        fig.savefig('G:\HK2_Nam 4\Digital\Project\Digital Image Processing Project\18110037_Le Trinh Hoang Phu_18110007_Huynh Quang Duy_19110064_Nguyen Dan Truong\Plate_detect_and_recognize\Fig plot\original_image.jpg')
        self.OriginalPlate.clear()
        self.OriginalPlate.setPixmap(QPixmap("G:\HK2_Nam 4\Digital\Project\Digital Image Processing Project\18110037_Le Trinh Hoang Phu_18110007_Huynh Quang Duy_19110064_Nguyen Dan Truong\Plate_detect_and_recognize\Fig plot\original_image.jpg"))

        fig = plt.figure(figsize=(12,7))
        plt.rcParams.update({"font.size":10})
        grid = gridspec.GridSpec(ncols=1,nrows=1,figure = fig)
        fig.add_subplot(grid[0])
        plt.axis(False)
        text = pytesseract.image_to_string(gray, config='--psm {0}'.format(int(self.PSM.text())))
        self.GrayResult.clear()
        self.GrayResult.insert(text)
        plt.title(text, fontdict={'fontsize' : 80})
        plt.imshow(gray, cmap ="gray")
        fig.savefig('G:\HK2_Nam 4\Digital\Project\Digital Image Processing Project\18110037_Le Trinh Hoang Phu_18110007_Huynh Quang Duy_19110064_Nguyen Dan Truong\Plate_detect_and_recognize\Fig plot\Gray.jpg')
        self.Gray.clear()
        self.Gray.setPixmap(QPixmap("G:\HK2_Nam 4\Digital\Project\Digital Image Processing Project\18110037_Le Trinh Hoang Phu_18110007_Huynh Quang Duy_19110064_Nguyen Dan Truong\Plate_detect_and_recognize\Fig plot\Gray.jpg"))

        fig = plt.figure(figsize=(12,7))
        plt.rcParams.update({"font.size":10})
        grid = gridspec.GridSpec(ncols=1,nrows=1,figure = fig)
        fig.add_subplot(grid[0])
        plt.axis(False)
        text = pytesseract.image_to_string(blur, config='--psm {0}'.format(int(self.PSM.text())))
        self.BlurResult.clear()
        self.BlurResult.insert(text)
        plt.title(text, fontdict={'fontsize' : 80})
        plt.imshow(blur, cmap ="gray")
        fig.savefig('G:\HK2_Nam 4\Digital\Project\Digital Image Processing Project\18110037_Le Trinh Hoang Phu_18110007_Huynh Quang Duy_19110064_Nguyen Dan Truong\Plate_detect_and_recognize\Fig plot\Blur.jpg')
        self.Blur.clear()
        self.Blur.setPixmap(QPixmap("G:\HK2_Nam 4\Digital\Project\Digital Image Processing Project\18110037_Le Trinh Hoang Phu_18110007_Huynh Quang Duy_19110064_Nguyen Dan Truong\Plate_detect_and_recognize\Fig plot\Blur.jpg"))

        fig = plt.figure(figsize=(12,7))
        plt.rcParams.update({"font.size":10})
        grid = gridspec.GridSpec(ncols=1,nrows=1,figure = fig)
        fig.add_subplot(grid[0])
        plt.axis(False)
        binary = cv2.bitwise_not(binary)
        text = pytesseract.image_to_string(binary, config='--psm {0}'.format(int(self.PSM.text())))
        self.BinaryResult.clear()
        self.BinaryResult.insert(text)
        plt.title(text, fontdict={'fontsize' : 80})
        plt.imshow(binary, cmap ="gray")
        fig.savefig('G:\HK2_Nam 4\Digital\Project\Digital Image Processing Project\18110037_Le Trinh Hoang Phu_18110007_Huynh Quang Duy_19110064_Nguyen Dan Truong\Plate_detect_and_recognize\Fig plot\Binary.jpg')
        self.Binary.clear()
        self.Binary.setPixmap(QPixmap("G:\HK2_Nam 4\Digital\Project\Digital Image Processing Project\18110037_Le Trinh Hoang Phu_18110007_Huynh Quang Duy_19110064_Nguyen Dan Truong\Plate_detect_and_recognize\Fig plot\Binary.jpg"))

        fig = plt.figure(figsize=(12,7))
        plt.rcParams.update({"font.size":10})
        grid = gridspec.GridSpec(ncols=1,nrows=1,figure = fig)
        fig.add_subplot(grid[0])
        plt.axis(False)
        dilation = cv2.bitwise_not(dilation)
        text = pytesseract.image_to_string(dilation, config='--psm {0}'.format(int(self.PSM.text())))
        self.DilationResult.clear()
        self.DilationResult.insert(text)
        plt.title(text, fontdict={'fontsize' : 80})
        plt.imshow(dilation, cmap ="gray")
        fig.savefig('G:\HK2_Nam 4\Digital\Project\Digital Image Processing Project\18110037_Le Trinh Hoang Phu_18110007_Huynh Quang Duy_19110064_Nguyen Dan Truong\Plate_detect_and_recognize\Fig plot\Dilation.jpg')
        self.Dilation.clear()
        self.Dilation.setPixmap(QPixmap("G:\HK2_Nam 4\Digital\Project\Digital Image Processing Project\18110037_Le Trinh Hoang Phu_18110007_Huynh Quang Duy_19110064_Nguyen Dan Truong\Plate_detect_and_recognize\Fig plot\Dilation.jpg"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
